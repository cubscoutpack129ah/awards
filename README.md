# What is this?

In our pack, it is helpful to produce labels each month for our awards
presentation.  Each label includes a Cub Scout's name, den number, and
a list of his awards.  We manage this data in a spreadsheet, but mail
merge broke down at some point.  The organization of the data became
somewhat variable, and the file moved between Excel, Google Sheets,
and Numbers.

The filter `clean.py` reads a monthly awards sheet exported to CSV
from standard input and writes a normalized representation to standard
output.  The normalized output looks like this:

    Smith,Alice,4,Hiking,Swimming
    Jones,Bob,2,Cooking,Cleaning

The filter `labels.py` reads normalized CSV data from standard input
and writes a PostScript program to generate label sheets on standard
output.

The filter `dens.py` reads the same normalized CSV data from standard
input and writes a LaTeX document containing a list of scouts and
their awards by den on standard output.

# How do I use this?

Assuming you have Python 2.7 or 3 and have exported a monthly awards
tab from the roster spreadsheet as `awards.csv`, do something like
this (Bash syntax for a Macintosh or other UNIX-like system):

    ./clean.py < awards.csv | ./labels.py > awards.ps

Then, send `awards.ps` to a PostScript-capable printer (or further
convert it to PDF if that's easier to print.)

On a Macintosh, the command line above can be turned into a
drag-and-drop Automater workflow, and PostScript is automatically
converted into PDF by Preview.

To print the page-per-den award lists, you'll need TeX Live installed.

# Tuning

These values in `labels.py` can be adjusted as needed:

## Labels per page

    ROWS_PER_PAGE = 4
    COLUMNS_PER_PAGE = 3

## Paper format

    PAGE_WIDTH_INCHES = 8.5
    PAGE_HEIGHT_INCHES = 11.

## Margins

    PAGE_MARGIN_INCHES = 1./2.
    CELL_MARGIN_INCHES = 1./8.

Cell size is derived from the above constraints, and font size is
derived from cell size.
