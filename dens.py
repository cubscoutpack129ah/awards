#!/usr/bin/env python

import csv
import sys

from collections import defaultdict


def make_den(den):

    den_number, scouts = den
    print("\\thispagestyle{empty}")    
    print('\\section*{{Den {}}}'.format(den_number))
    print("\\begin{itemize}")
    for scout in sorted(scouts):
        for last_name, first_name, awards in scouts:
            print("\\item {} {}".format(first_name, last_name))
            print("\\begin{itemize}")
            for award in sorted(awards):
                print("\\item {}".format(award))
            print("\\end{itemize}")
    print("\\end{itemize}")
    print("\\clearpage")


def make_pages(flo):

    dens = defaultdict(list)
    rdr = csv.reader(flo)
    for row in rdr:
        last_name, first_name, den_number, *awards, = row
        dens[int(den_number)].append((last_name, first_name, awards,))
    print("\\documentclass[12pt]{report}")
    print("\\begin{document}")
    for den in sorted(dens.items()):
        make_den(den)
    print("\\end{document}")


if __name__ == '__main__':

    make_pages(sys.stdin)
