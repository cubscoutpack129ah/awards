#!/usr/bin/env python

import logging
import re
import sys


AWARDS_OFFSET = 4


def extract_award_names(flo):

    award_names = []
    while len(award_names) == 0:
        top_header_fields = flo.readline().split(',')
        award_names = [x.strip() for x in top_header_fields[AWARDS_OFFSET:] if x.strip()]
    return award_names


def extract_records(flo, award_names):

    records = []
    first_name_chars, last_name_chars = 0, 0
    for line in sys.stdin.readlines():
        line = line.strip()
        record = line.split(',')
        while not len(record) > 0 and record[-1].strip():
            record = record[:-1]
        if len(record) < 3:
            logging.warning('Record too short to make sense, skipping it.')
            continue

        first_name = record[2].strip()
        # Avoid partial header rows.
        if not (first_name and first_name != 'FirstName'):
            logging.warning('Extraneous, skipping line: %s' % (line,))
            continue

        m = re.search('[Dd]en\s*(\d+)', record[1])
        if not m:
            logging.warning('Failed to parse den number, skipping: %s' % (line,))
            continue
        den = m.group(1)

        last_name = record[3].strip()

        awards = []
        awards = [award_names[x] for x in range(len(record)-AWARDS_OFFSET) if record[x+AWARDS_OFFSET].strip()]
        if len(awards) == 0:
            logging.warning('No awards, skipping: %s' % (line,))
            continue

        records.append([last_name, first_name, den, ] + awards)
        first_name_chars += len(first_name)
        last_name_chars += len(last_name)

    # Sometimes the first name and last name columns are switched...
    if first_name_chars > last_name_chars:
        for record in records:
            record[0], record[1] = record[1], record[0]

    return records


if __name__ == '__main__':

    award_names = extract_award_names(sys.stdin)
    records = extract_records(sys.stdin, award_names)

    for record in records:
        print(','.join(record))
