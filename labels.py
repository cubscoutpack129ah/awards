#!/usr/bin/env python

import csv
import sys

ROWS_PER_PAGE = 4
COLUMNS_PER_PAGE = 3

PAGE_WIDTH_INCHES = 8.5
PAGE_HEIGHT_INCHES = 11.
PAGE_MARGIN_INCHES = 1./2.
CELL_MARGIN_INCHES = 1./8.

CELLS_PER_PAGE = ROWS_PER_PAGE * COLUMNS_PER_PAGE
PAGE_MARGIN = PAGE_MARGIN_INCHES * 72
DRAWING_AREA_WIDTH = (PAGE_WIDTH_INCHES - 2 * PAGE_MARGIN_INCHES) * 72
DRAWING_AREA_HEIGHT = (PAGE_HEIGHT_INCHES - 2 * PAGE_MARGIN_INCHES) * 72
CELL_WIDTH = DRAWING_AREA_WIDTH / COLUMNS_PER_PAGE
CELL_HEIGHT = DRAWING_AREA_HEIGHT / ROWS_PER_PAGE
CELL_MARGIN = CELL_MARGIN_INCHES * 72


def make_grid(flo):

    print("newpath")

    for i in range(COLUMNS_PER_PAGE + 1):
        x = PAGE_MARGIN + i * CELL_WIDTH
        y0 = PAGE_MARGIN
        y1 = y0 + DRAWING_AREA_HEIGHT
        print("%d %d moveto %d %d lineto" % (x, y0, x, y1,))

    for i in range(ROWS_PER_PAGE + 1):
        x0 = PAGE_MARGIN
        x1 = x0 + DRAWING_AREA_WIDTH
        y = PAGE_MARGIN + i * CELL_HEIGHT
        print("%d %d moveto %d %d lineto" % (x0, y, x1, y,))

    print("0.8 setgray 0.5 setlinewidth stroke")


def make_den_number(flo, i, n):

    right = CELL_WIDTH * ((i % COLUMNS_PER_PAGE) + 1) + PAGE_MARGIN - CELL_MARGIN
    top = CELL_HEIGHT * ((i // COLUMNS_PER_PAGE) + 1) + PAGE_MARGIN - CELL_MARGIN
    fs = CELL_HEIGHT / 6

    print("/Helvetica findfont %d scalefont setfont 0.8 setgray" % (fs,))
    print("(%s) dup stringwidth pop %d exch sub %d moveto show" % (n, right, top - fs,))


def make_name(flo, i, fname, lname):

    left = CELL_WIDTH * (i % COLUMNS_PER_PAGE) + PAGE_MARGIN + CELL_MARGIN
    top = CELL_HEIGHT * ((i // COLUMNS_PER_PAGE) + 1) + PAGE_MARGIN - CELL_MARGIN
    fs = CELL_HEIGHT / 12

    print("/Helvetica findfont %d scalefont setfont 0 setgray" % (fs,))
    print("%d %d moveto (%s) show %d %d moveto (%s) show" % (left, top - fs, fname, left, top - (2 * fs), lname.upper(),))


def make_awards(flo, i, awards):

    name_fs = CELL_HEIGHT / 12
    fs = CELL_HEIGHT / 16
    left = CELL_WIDTH * (i % COLUMNS_PER_PAGE) + PAGE_MARGIN + CELL_MARGIN
    top = CELL_HEIGHT * ((i // COLUMNS_PER_PAGE) + 1) + PAGE_MARGIN - CELL_MARGIN - (3.5 * name_fs)

    print("/Helvetica findfont %d scalefont setfont 0 setgray" % (fs,))
    for a in awards:
        if len(a) > 0:
            print("%d %d moveto (%s) show" % (left, top, a,))
            top -= (fs + 2)


def make_pages(flo):

    print("%!PS")
    print("%%Title: Cub Scout award labels")
    rdr = csv.reader(flo)
    for n, row in enumerate(rdr):
        i = n % CELLS_PER_PAGE
        if i == 0:
            if n > 0:
                print("showpage")
            make_grid(flo)
        make_den_number(flo, i, row[2])
        make_name(flo, i, row[1], row[0])
        make_awards(flo, i, row[3:])
    print("showpage")


if __name__ == '__main__':

    make_pages(sys.stdin)
